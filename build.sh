#!/bin/bash

echo "Preparing files for Open Architect Map front-end"
rm -rf owmf/front-end/out
cp .env.example owmf/front-end/.env
cp i18n.json owmf/front-end/i18n.json

echo "Installing front-end dependencies"
cd owmf/front-end
npm i

echo "Building front-end"
npm run build
